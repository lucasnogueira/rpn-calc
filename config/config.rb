require_relative '../lib/interface.rb'
require_relative '../lib/processor.rb'
require_relative '../lib/calculator.rb'
require_relative '../lib/extensions/string.rb'
require_relative '../lib/extensions/exceptions.rb'

require 'readline'
