class Processor
  # Constant containing the currently supported operators.
  OPERATORS = %w(+ - / *)

  # Private: Initializes the Processor
  def initialize
    @operands = Array.new
  end

  # Public: Starts the process and manage the calculations
  # between the inputs.
  #
  # input - A String containing the user input.
  #
  # Returns nil
  def run(input)
    input.is_number? ? @operands.push(input.to_f) : set_operator(input)

    if calculate?
      input = calculate
      @operands.push(input.to_f)
      @operator = nil
    end

    puts input
  end

  private

  # Private: Checks if it is the right turn to start a
  # calculation
  #
  # Raises InsufficientValuesError if the operands stack size is lower than 2
  # and the current input is an operator.
  #
  # Returns a String or nil.
  def calculate?
    if @operands.size < 2 && @operator
      @operator = nil
      raise InsufficientValuesError
    end

    @operator
  end

  # Private: Intializes the Calculator and call the calculate.
  #
  # Returns a Float.
  def calculate
    Calculator.new(@operands.pop(2), @operator).calculate
  end

  # Private: Assigns @operator with the input value.
  #
  # Raises InvalidOperatorError if the operator isn't supported.
  #
  # Returns a String.
  def set_operator(input)
    unless OPERATORS.include?(input)
      raise InvalidOperatorError
    end

    @operator = input
  end
end
