class Calculator
  # Private: Initializes the Calculator
  #
  # operands - Array containing 2 floats.
  # operator - String containing an operator symbol.
  def initialize(operands, operator)
    @first_operand, @second_operand = operands
    @operator  = operator
  end

  # Public: Process the calculation between the 2 operands
  # and the operator.
  #
  # Returns a Float.
  def calculate
    @first_operand.send(@operator, @second_operand)
  end
end
