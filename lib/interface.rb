class Interface
  # Public: Initiates the application, reading through the user inputs
  # and initialiazes a Processor to handle the inputs.
  #
  # Returns nil.
  def run
    processor = Processor.new

    while true
      begin
        input = Readline.readline("> ")
        break if input.nil? || input.include?('q')

        if input.size > 1
          input.split.each do |i|
            processor.run(i)
          end
        else
          processor.run(input)
        end

      rescue InvalidOperatorError => e
        puts e.message

      rescue InsufficientValuesError => e
        puts e.message
      end
    end
  end
end
