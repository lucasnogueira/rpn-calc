class InvalidOperatorError < StandardError
  # Public: Overwrites the default message, adding a custom value to it.
  #
  # Returns a String.
  def message
    super + '. The currently supported list is: "+", "-", "/" and "*".'
  end
end

class InsufficientValuesError < StandardError
  # Public: Overwrites the default message, adding a custom value to it.
  #
  # Returns a String.
  def message
    super + '. Insufficient values in the expression.'
  end
end
