class String
  # Public: Checks if self is a number.
  #
  # Returns a Boolean.
  def is_number?
    self.to_f.to_s == self || self.to_i.to_s == self
  end
end
