require 'spec_helper'

describe Calculator do
  let(:calculator) { Calculator.new([10.0, 2.0], operator) }

  subject { calculator.calculate }

  describe 'calculate' do
    context '+' do
      let(:operator) { '+' }

      it { is_expected.to eq 12.0 }
    end

    context '-' do
      let(:operator) { '-' }

      it { is_expected.to eq 8.0 }
    end

    context '/' do
      let(:operator) { '/' }

      it { is_expected.to eq 5.0 }
    end

    context '*' do
      let(:operator) { '*' }

      it { is_expected.to eq 20.0 }
    end
  end
end
