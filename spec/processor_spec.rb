require 'spec_helper'

describe Processor do
  describe 'run' do
    let(:processor) { Processor.new }

    context 'when input is a number' do
      subject { processor.run('1') }

      it 'adds to the operands stack' do
        subject
        expect(processor.instance_variable_get(:@operands)).to eq([1.0])
      end

      it 'prints the input' do
        expect(STDOUT).to receive(:puts).with('1')
        subject
      end
    end

    context 'when input is not a number' do
      context 'with invalid operator' do
        subject { processor.run('L') }

        it 'raises error' do
          expect { subject }.to raise_error(InvalidOperatorError)
        end
      end

      context 'with valid operator' do
        subject { processor.run('+') }

        context 'with insufficient values' do
          it 'raises error' do
            expect { subject }.to raise_error(InsufficientValuesError)
          end
        end

        context 'with sufficient values' do
          before { processor.instance_variable_set(:@operands, [2.0, 1.0]) }

          it 'prints the calculation' do
            expect(STDOUT).to receive(:puts).with(3.0)
            subject
          end

          it 'adds the result to the operands stack' do
            subject
            expect(processor.instance_variable_get(:@operands)).to eq([3.0])
          end
        end
      end
    end
  end
end
