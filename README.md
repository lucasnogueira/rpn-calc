# RPN CALCULATOR

## Solution
The calculator will accept only the 4 basic operations (+, -, / and *) and need at least 2 number inputs to process the result. If just 1 number input and 1 operator input are provided, the calculator will return an error message. Notice that the result of a calculation counts as a number input.

The user is free to add the inputs one by one or together, as long as they are separated by spaces.

## Technical Choices

When I started to code I had the impression that the operands and operators would play the major whole in the application, so I created separated instances for them. After some time I noticed this was not true, so I was forced to refactor the code. In the end I separated the interface, so it is possible to extend/change it in the future, created a processor (the core of the system) to deal with the stack management and to control the calculation flow. The calculator is also a separated class designated to process the operations and only this.

I created a directory to hold some generic changes, called "extensions". Here I have a file to extend the String class in order to make it easier to check if the input is a number or a operator symbol, and also a file to contain all the exceptions. I could display them inside one of the classes mentioned before, but since I created this directory I thought I could keep a file here for this.

The spec directory have the test files and the config one has a single file that requires all the files in order for the system to work properly.

The only external dependency is the Rspec gem. I know there are some gems that could have helped in the process, but I wanted to do this as clean as possible.

## Trade-offs

When I talked to Alex he said this should be simple and not took too much time, so I set a challenge to do this using 4 hours max (not counting this README :P), so there are some things I would do if I had more time.

First, since I started with the concept of Operators and Operands as separated classes, I wasted some time there and with the refactor, so I believe the operator code could be improved. I think I could come with a refactor to the Processor as well, in order to move some code from there, maybe creating another instance.

Also, I tried to test the interface (cli) with Rspec but was a pain. I searched and found that Aruba(https://github.com/cucumber/aruba) is used for this kind of application, but I decided to not test the interface because:

- I never used Aruba before, so I would have to spend some time learning, etc.
- I have used Cucumber in the past, and it is very verbose. This would affect my 4 hours goal for sure.

## How to run the code

I set the ruby version for this app to 2.4.1. If you don't have it installed you will have to install it before running the app. Also, it is necessary to install the gem bundler (gem install bundler) and run `bundle install` to install the test dependencies.

With this set, all you have to do is run `ruby rpn_calculator.rb` on your terminal.

For the tests, run `rspec spec/` on the terminal.
